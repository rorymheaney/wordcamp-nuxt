export default {
    basePostsUrl: 'wp/v2/posts',
	basePagesUrl: 'wp/v2/pages',
	mainMenuUrl: 'menus/v1/menus/2' // menu ID goes here
}
