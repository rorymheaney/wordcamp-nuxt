const redirects = [
	/* Redirect option here */
	{
		from: "^/old-contact",
		to: "/contact/",
		statusCode: 301
	}
]
module.exports = redirects;
